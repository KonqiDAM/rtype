/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r.type;

import javafx.application.Application;
import javafx.stage.Stage;
import r.type.sprite.*;
import r.type.scene.*;
/**
 *
 * @author void
 */
public class RType extends Application {

    public static final int MAX_SCENES = 3;

    public static final int WELCOME_SCENE = 0;
    public static final int GAME_SCENE = 1;
    public static final int GAME_OVER_SCENE = 2;

    public static final RTypeScene[] scenes = new RTypeScene[MAX_SCENES];

    private static Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;

        scenes[0] = new WelcomeScene();
        scenes[1] = new GameScene();
        scenes[2] = new GameOverScene();


        primaryStage.setTitle("R-Type");
        setScene(WELCOME_SCENE);
        primaryStage.show();
    }

    public static void setScene(int numScene) {
        primaryStage.setScene(scenes[numScene]);
        scenes[numScene].draw();
    }

    public static void exit() {
        primaryStage.hide();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
