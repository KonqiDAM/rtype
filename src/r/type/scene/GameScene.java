/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r.type.scene;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.animation.AnimationTimer;
import javafx.scene.input.KeyCode;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import r.type.RType;
import r.type.level.Level;
import r.type.sprite.*;

/**
 *
 * @author kek
 */
public class GameScene extends RTypeScene {

    private boolean isPlayerAlive;
    public static final long timer = 500000000;

    private MainShip player;
    private Level level;
    private Font gameFont;
    private Random random;
    private long timePressedSpace;
    private long timeLastEnemy;
    private int coolDown;//Game goes a bit to fast in linux, adjust to a nice speed.
    private int currentLevel;

    boolean spacePressed;

    ArrayList<Wall> walls;
    ArrayList<Shot> playerShots;
    ArrayList<EnemyShip> enemies;
    ArrayList<EnemyShot> enemyShots;
    ArrayList<EnemyWalker> enemyWalkers;
    ArrayList<HealthRestore> healtPacks;

    public GameScene() {
        super();
        isPlayerAlive = true;
        random = new Random();
        gameFont = Font.font("Courier New", FontWeight.BOLD, 24);
        gc.setFont(gameFont);
        player = new MainShip();
        player.moveTo(20, 20);
        walls = new ArrayList<Wall>();
        playerShots = new ArrayList<Shot>();
        enemyShots = new ArrayList<>();
        walls.add(new Wall(1));
        walls.add(new Wall(2));
        walls.add(new Wall(3));

        walls.get(0).moveTo(GAME_WIDTH, GAME_HEIGHT - 44);
        walls.get(1).moveTo(GAME_WIDTH + 180, GAME_HEIGHT - 44);
        walls.get(2).moveTo(GAME_WIDTH + 360, GAME_HEIGHT - 44);

        enemies = new ArrayList<>();
        enemies.add(new EnemyShip(GAME_WIDTH - 50, 500));
        enemyWalkers = new ArrayList<>();
        healtPacks = new ArrayList<>();
        coolDown = 1;

    }

    @Override
    public void draw() {
        currentLevel = 0;
        activeKeys.clear();
        releasedKeys.clear();
        timeLastEnemy = 0;
        new AnimationTimer() {
            @Override
            public void handle(long now) {
                if (coolDown > 0) {
                    coolDown--;
                } else {
                    coolDown = 1;
                    detectKeys(this, now);
                    drawScreen();
                    moveObjects();
                    PickHealth();
                    if (!isPlayerAlive) {
                        enemies.clear();
                        enemyShots.clear();
                        player.setLives(3);
                        player.moveTo(20, 500);
                        isPlayerAlive = true;
                        this.stop();
                        RType.setScene(RType.GAME_OVER_SCENE);
                    }
                    generateEnemies(now);
                }
            }
        }.start();
    }

    private void generateEnemies(long now) {

        if ((now - timeLastEnemy) / (timer / 2) > 3) {
            enemyWalkers.forEach((e) -> {
                enemyShots.add(new EnemyShot(player.getX(),
                        player.getY() , e.getX(), e.getY()));
            });
        }

        if (((now - timeLastEnemy) / timer > 3)) {
            if (enemies.size() > 4) {
                for (int i = 0; i < enemies.size() / 3; i++) {
                    int rnd = random.nextInt(enemies.size());
                    enemyShots.add(new EnemyShot(player.getX(),
                            player.getY(),
                            enemies.get(rnd).getX(),
                            enemies.get(rnd).getY()
                    ));
                }
            } else {
                for (int i = 0; i < enemies.size(); i++) {
                    enemyShots.add(new EnemyShot(player.getX(),
                            player.getY(),
                            enemies.get(i).getX(),
                            enemies.get(i).getY()
                    ));
                }
            }

            EnemyShip aux = new EnemyShip(GAME_WIDTH - 50,
                    random.nextInt(GAME_HEIGHT - 100));

            enemies.add(aux);
            timeLastEnemy = now;
        }

    }

    private void detectKeys(AnimationTimer timer, long now) {

        if (activeKeys.contains(KeyCode.SPACE)) {
            if (!spacePressed) {
                spacePressed = true;
                timePressedSpace = now;
            }
        }

        if (activeKeys.contains(KeyCode.ESCAPE)) {
            timer.stop();
            RType.setScene(RType.GAME_OVER_SCENE);
        }

        if (releasedKeys.contains(KeyCode.SPACE)) {
            releasedKeys.remove(KeyCode.SPACE);
            playerShots.add(new Shot((now - timePressedSpace)));
            playerShots.get(playerShots.size() - 1).moveTo(player.getX()
                    + player.getWidth(), player.getY()
                    - playerShots.get(playerShots.size() - 1).getHeiht() / 2
                    + player.getHeiht() / 2);
            spacePressed = false;
        }

        if (activeKeys.contains(KeyCode.A)) {
            player.MoveLeft();
        }

        if (activeKeys.contains(KeyCode.S)) {
            player.MoveDown();
        }

        if (activeKeys.contains(KeyCode.D)) {
            player.MoveRigth();
        }

        if (activeKeys.contains(KeyCode.W)) {
            player.MoveUp();
        }
    }

    private void drawHUD() {
        gc.setFill(Color.BLACK);
        gc.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        gc.setFill(Color.GREEN);
        gc.fillText("Lives: " + player.getLives(), 5, 20);
    }

    
    private void PickHealth()
    {
        for (int i = 0; i < healtPacks.size(); i++) {
            if(healtPacks.get(i).collidesWith(player))
            {
                player.setLives(player.getLives()+1);
                healtPacks.remove(i);
            }
        }
    }
    
    private void drawScreen() {
        drawHUD();

        playerShots.forEach((s) -> {
            s.draw(gc);
        });

        enemies.forEach((e) -> {
            e.draw(gc);
        });

        enemyShots.forEach((e) -> {
            e.draw(gc);
        });

        player.draw(gc);

        walls.forEach((w) -> {
            w.draw(gc);
        });

        enemyWalkers.forEach((w) -> {
            w.draw(gc);
        });
        
        healtPacks.forEach((w) -> {
            w.draw(gc);
        });

    }

    private void moveWalls() {

        int i = 0;
        while (i < walls.size()) {
            walls.get(i).MoveLeft();

            if (walls.get(i).collidesWith(player)) {
                isPlayerAlive = false;
            }

            if (walls.get(i).getX() + 180 < 0) {
                walls.get(i).moveTo(GAME_WIDTH, GAME_HEIGHT - 44);
                if (i == 1 && enemyWalkers.isEmpty()) {
                    enemyWalkers.add(new EnemyWalker(GAME_WIDTH, GAME_HEIGHT - 131));
                }
            }
            i++;
        }
    }

    private void collisionShotsWalls() {
        for (int i = 0; i < playerShots.size(); i++) {
            for (Wall w : walls) {
                if (playerShots.get(i).collidesWith(w)) {
                    playerShots.remove(playerShots.get(i));
                    break;
                }
            }
        }
    }

    private void collisionShotsEnemies() {
        for (int i = 0; i < playerShots.size(); i++) {
            for (int j = 0; j < enemies.size(); j++) {
                if (enemies.get(j).collidesWith(playerShots.get(i))) {
                    enemies.get(j).setHealth(enemies.get(j).getHealth()
                            - playerShots.get(i).getDamage());

                    if (enemies.get(j).getHealth() <= 0) {
                        enemies.remove(enemies.get(j));
                    }
                    playerShots.remove(playerShots.get(i));
                    break;
                }
            }
        }
        for (int i = 0; i < playerShots.size(); i++) {
            for (int j = 0; j < enemyWalkers.size(); j++) {
                if (enemyWalkers.get(j).collidesWith(playerShots.get(i))) {
                    enemyWalkers.get(j).setHealth(enemyWalkers.get(j).getHealth()
                            - playerShots.get(i).getDamage());

                    if (enemyWalkers.get(j).getHealth() <= 0) {
                        healtPacks.add(new HealthRestore(enemyWalkers.get(j).getX(),
                                enemyWalkers.get(j).getY()));
                        enemyWalkers.remove(enemyWalkers.get(j));

                    }
                    playerShots.remove(playerShots.get(i));
                    break;
                }
            }
        }
    }

    private void movePlayerShots() {

        for (int i = 0; i < playerShots.size(); i++) {
            playerShots.get(i).MoveRigth();

            if (playerShots.get(i).checkOutOfScreen()) {
                playerShots.remove(playerShots.get(i));
            }

        }
        collisionShotsEnemies();
        collisionShotsWalls();
    }

    private void moveHealthPack()
    {
        healtPacks.forEach((h) -> {
            h.MoveLeft();
        });
        
    }
    
    private void moveEnemyShots() {
        int i = 0;
        while (i < enemyShots.size()) {
            enemyShots.get(i).Move();
            if (enemyShots.get(i).collidesWith(player)) {
                player.setLives(player.getLives() - 1);
                if (player.getLives() <= 0) {
                    isPlayerAlive = false;
                }
                enemyShots.remove(i);
            }
            i++;
        }

        for (i = 0; i < enemyShots.size(); i++) {
            if (enemyShots.get(i).checkOutOfScreen()) {
                enemyShots.remove(i);
            }
        }

        i = 0;
        while (i < enemyShots.size()) {
            for (Shot s : playerShots) {
                if (s.collidesWith(enemyShots.get(i))) {
                    enemyShots.remove(i);
                    playerShots.remove(s);
                    break;
                }
            }

            i++;
        }
    }

    private void moveEnemies() {

        enemyWalkers.stream().map((e) -> {
            e.MoveLeft();
            return e;
        }).map((e) -> {
            if (e.collidesWith(player)) {
                isPlayerAlive = false;
            }
            return e;
        }).filter((e) -> (e.checkOutOfScreen())).forEachOrdered((e) -> {
            e.moveTo(GAME_WIDTH - 1, GAME_HEIGHT - 131);
        });

        enemies.stream().map((e) -> {
            e.MoveLeft();
            return e;
        }).map((e) -> {
            if (e.checkOutOfScreen()) {
                e.moveTo(GAME_WIDTH - 50, random.nextInt(GAME_HEIGHT - e.getHeiht()));
            }
            return e;
        }).filter((e) -> (e.collidesWith(player))).forEachOrdered((_item) -> {
            isPlayerAlive = false;
        });
    }

    private void moveObjects() {
        moveWalls();
        movePlayerShots();
        moveEnemyShots();
        moveEnemies();
        moveHealthPack();
    }
}
