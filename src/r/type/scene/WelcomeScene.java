/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r.type.scene;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import javafx.animation.AnimationTimer;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import r.type.RType;
/**
 *
 * @author void
 */
public class WelcomeScene extends RTypeScene {
    public static final String WELCOME_SCREEN_PATH = "img/WelcomeImg.png";
    
    private Image imgBackground;
    
    public WelcomeScene()
    {        
        super();
        
        try
        {
            imgBackground = new Image(Files.newInputStream(Paths.get(WELCOME_SCREEN_PATH)));
        } catch (Exception e) {
        }                
    }

    @Override
    public void draw() 
    {
        activeKeys.clear();

        new AnimationTimer()
        {
            public void handle(long currentNanoTime)
            {
                if(activeKeys.contains(KeyCode.SPACE))
                {
                    activeKeys.remove(KeyCode.SPACE);
                    this.stop();
                    RType.setScene(RType.GAME_SCENE);
                } else if (activeKeys.contains(KeyCode.ESCAPE)) {
                    this.stop();
                    RType.exit();
                }
                gc.drawImage(imgBackground, 0, 0);        
            }
        }.start();        
    }
}
