/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r.type.scene;

import java.nio.file.Files;
import java.nio.file.Paths;
import javafx.animation.AnimationTimer;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import r.type.RType;

/**
 *
 * @author void
 */
public class GameOverScene extends RTypeScene {

    public static final String GAME_OVER_SCREEN_PATH = "img/GameOver.png";
    
    private Image imgBackground;
    
    public GameOverScene()
    {        
        super();
        
        try
        {
            imgBackground = new Image(Files.newInputStream(Paths.get(GAME_OVER_SCREEN_PATH)));
        } catch (Exception e) {
        }                
    }
    
    
    @Override
    public void draw() 
    {
        activeKeys.clear();
        
        new AnimationTimer()
        {
            @Override
            public void handle(long currentNanoTime)
            {
                if(activeKeys.size() > 0)
                {
                    this.stop();
                    RType.setScene(RType.WELCOME_SCENE);
                }
                gc.drawImage(imgBackground, 0, 0);        
            }
        }.start();        
    }  
    
}
