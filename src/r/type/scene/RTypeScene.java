/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r.type.scene;

import java.util.*;
import javafx.scene.*;
import javafx.scene.canvas.*;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 *
 * @author void
 */
public abstract class RTypeScene extends Scene {

    public static final int GAME_WIDTH = 1000;
    public static final int GAME_HEIGHT = 600;
    
    private StackPane root = new StackPane();

    protected Set<KeyCode> activeKeys;
    protected Set<KeyCode> releasedKeys;

    protected GraphicsContext gc;
        
    public RTypeScene() 
    {
        super(new StackPane(), GAME_WIDTH, GAME_HEIGHT);
        root = new StackPane();
        this.setRoot(root);
                
        Canvas canvas = new Canvas(GAME_WIDTH, GAME_HEIGHT);
        root.getChildren().add(canvas);
        gc = canvas.getGraphicsContext2D();        

        activeKeys = new HashSet<>();
        releasedKeys = new HashSet<>();
        
        this.setOnKeyPressed(e -> {
            activeKeys.add(e.getCode());
        });
        this.setOnKeyReleased(e -> {
            activeKeys.remove(e.getCode());
            releasedKeys.add(e.getCode());
        });    
    }
    
    public abstract void draw();
}
