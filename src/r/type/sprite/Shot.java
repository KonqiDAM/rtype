/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r.type.sprite;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import javafx.scene.image.Image;

/**
 *
 * @author void
 */
public class Shot extends Sprite {
    
    protected int damage;
    protected long chargeTime = 100000000;//0.1 seconds
    public Shot(long time) {
        super("img/shot3.png", 18, 18);
        int aux;
        speed = 6;
        damage = 10+(int)(time/chargeTime);
        if(damage < 15)
        {
            aux = 1;
            this.width = 42;
            this.height = 12;
        }
        else if (damage < 25)
        {
            aux = 2;
            this.width = 43;
            this.height = 36;
        }
        else
        {
            this.width = 214;
            this.height = 42;
            aux = 3;
        }
        
        
        try
        {
            sprite = new Image(Files.newInputStream(Paths.get("img/shot"+aux+".png")));
        } catch (IOException e) {
        } 
            
        
        //System.out.println(damage);
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}
