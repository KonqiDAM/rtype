/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r.type.sprite;

/**
 *
 * @author void
 */
public class EnemyShip extends Sprite {

    protected int health;
    public static final String ENEMY_SHIP_IMG = "img/enemy1.png";

    
    public EnemyShip(int x, int y) {
        super(ENEMY_SHIP_IMG, 37, 48);
        speed = 2;
        this.x = x;
        this.y = y;
        health = 20;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
}
