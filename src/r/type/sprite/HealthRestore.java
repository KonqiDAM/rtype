/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r.type.sprite;

/**
 *
 * @author void
 */
public class HealthRestore extends Sprite{

    public static final String HEALTH_IMG = "img/HealthPack.png";

    public HealthRestore(int x, int y) {
        super(HEALTH_IMG, 64, 60);
        speed = 1;
        this.x = x;
        this.y = y;
    }

    
    
}
