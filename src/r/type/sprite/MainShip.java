/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r.type.sprite;

import r.type.scene.RTypeScene;

/**
 *
 * @author void
 */
public class MainShip extends Sprite {

    public static final int MAX_LIVES = 3;
    private int lives;
    public static final String MAIN_SHIP_IMG = "img/MainShip.png";

    public MainShip() {
        super(MAIN_SHIP_IMG, 69, 33);
        this.lives = MAX_LIVES;
        this.speed = 4;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }
        
    @Override
    public void MoveRigth() {
        if (x + speed < RTypeScene.GAME_WIDTH - width) {
            x += speed;
        }
    }
    
    @Override
    public void MoveLeft() {
        if (x - speed > 0) {
            x -= speed;
        }
    }
    
    @Override
    public void MoveUp() {
        if (y - speed > 0) {
            y -= speed;
        }
    }
    
    @Override
    public void MoveDown() {
        if(y+speed<RTypeScene.GAME_HEIGHT-height)
        y += speed;
    }
}
