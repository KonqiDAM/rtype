/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r.type.sprite;

import static java.lang.Math.abs;

/**
 *
 * @author void
 */
public class EnemyShot extends Sprite {

    protected int xSpeed, ySpeed;

    public EnemyShot(int xPlayer, int yPlayer, int x, int y) {
        super("img/enemyShot.png", 18, 18);

        this.x = x;
        this.y = y;
        speed = 8;
        xSpeed = xPlayer < x ? -speed : speed;
        ySpeed = yPlayer < y ? -speed : speed;
        double angle = Math.toDegrees(Math.atan2(-yPlayer + y, -xPlayer + x));
        if(angle < 0)
            angle = -angle;
        if( angle > 90)
            angle = 180 - angle;
        
        xSpeed = (int) (((90 - angle) / 90) * xSpeed);
        ySpeed = (int) (((angle) / 90) * ySpeed);
    }

    public void Move() {
        x += xSpeed;
        y += ySpeed;
    }
}
