/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r.type.sprite;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import r.type.scene.RTypeScene;

/**
 *
 * @author void
 */
public class Sprite {
    
    protected Image sprite;
    protected int x, y, width, height, speed;
    protected boolean isOutOfscreen;
    
    public Sprite(String imgRoute, int width, int height)
    {
        this.width = width;
        this.height = height;
        this.speed = 1;
        try
        {
            sprite = new Image(Files.newInputStream(Paths.get(imgRoute)));
        } catch (IOException e) {
        }                        
    }
    
    public void moveTo(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    public boolean checkOutOfScreen()
    {
        isOutOfscreen = x < 0-width || x>RTypeScene.GAME_WIDTH 
                || y > RTypeScene.GAME_HEIGHT || y < 0 -height;
        
        return isOutOfscreen;
    }
    
    public int getX()
    {
        return x;
    }
    
    public int getY()
    {
        return y;
    }

    public int getWidth()
    {
        return width;
    }
    
    public int getHeiht()
    {
        return height;
    }
    
    public void draw(GraphicsContext gc)
    {
        gc.drawImage(sprite, x, y);
        
    }
    
    public boolean collidesWith(Sprite sp)
    {
        return (x + width > sp.getX() && x < sp.getX() + sp.getWidth() && 
                y + height > sp.getY() && y < sp.getY() + sp.getHeiht());
    }
    
    public void MoveRigth()
    {
        x += speed;
    }
    
    public void MoveLeft()
    {
        x -= speed;
    }
    
    public void MoveUp()
    {
        y -= speed;
    }
    
    public void MoveDown()
    {
        y += speed;
    }
}
