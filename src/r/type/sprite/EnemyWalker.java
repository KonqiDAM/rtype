/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r.type.sprite;

/**
 *
 * @author void
 */
public class EnemyWalker extends Sprite {

    protected int health;
    public static final String ENEMY_WALKER_IMG = "img/Walker.png";

    
    public EnemyWalker(int x, int y) {
        super(ENEMY_WALKER_IMG, 86, 87);
        speed = 1;
        this.x = x;
        this.y = y;
        health = 50;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    } 
    
}
